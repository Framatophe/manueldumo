# Mes messages sur Internet

La communication est l'essence même d'Internet. Le réseau a été construit sur la base de protocoles de communication ouverts et accessibles à tous. Pour envoyer des messages, les utilisateurs peuvent utiliser plusieurs solutions logicielles. Dans ce chapitre, nous allons nous pencher sur les principaux usages&nbsp;: le courrier électronique et ses pratiques, les webmails, les clients de courriel, la messagerie instantanée et les principaux protocoles concernés. Nous aurons aussi l'occasion de toucher quelques questions de sécurité.

## Le courrier électronique


 «&nbsp;Vous avez reçu un courriel&nbsp;»[^11]. Pour beaucoup, ce message est devenu très banal. Que ce soit dans un cadre professionnel ou personnel, l'envoi et la réception de courriers électroniques sont devenus des activités à part entière, comme planifier une réunion ou aller chercher son pain. Pour certaines personnes, les «&nbsp;méls&nbsp;»  peuvent être rares voire inexistants, mais les institutions de l'État ou les acteurs économiques en général incitent de plus en plus à se passer de correspondance papier.

De manière commune, le courrier électronique se compare assez facilement à la lettre que l'on envoie via le service postal au coin de la rue. Cependant, le seul fait de communiquer par voie numérique implique des changements bien plus profonds. En effet, la comparaison avec la lettre sous enveloppe s'arrête là. Un courrier électronique est copié plusieurs fois sur les serveurs où il transite et tous les administrateurs de ces serveurs peuvent lire le courrier, un peu comme s'il s'agissait plutôt d'une carte postale. Si on veut une enveloppe, il faut *chiffrer* le contenu du courriel (voir la section consacrée au chiffrement). Faisons un petit tour des solutions de courrier électronique.

## Les services de messagerie en ligne

Un service de messagerie en ligne est un service accessible via un navigateur Web. Une connexion est demandée, avec un nom d'utilisateur et un mot de passe.

Dans leur grande majorité, les services de courriel disposent d'une version en ligne, souvent appelée *webmail*, mais il est possible d'accéder à ses messages via un logiciel client de courriel (voir plus bas). On accède généralement à ces webmails sur le portail du fournisseur.

L'avantage d'un *webmail* est de pouvoir accéder à sa messagerie depuis n'importe quel ordinateur connecté. Parmi les nombreux services de messagerie en ligne, on peut distinguer les plus connus, qui mêlent à la fois un service d'émission et de réception de courriel, de la messagerie instantanée (*chat*), du stockage de fichiers en ligne, et des outils de réseaux sociaux. L'ensemble de ces services, accessibles depuis un compte unique, forme un *cloud (nuage)* où il est possible de travailler sans avoir à stocker quoi que ce soit localement sur un ordinateur. Ajoutés à ce dispositif, on peut mentionner des systèmes d'édition de documents (bureautique) ou des systèmes de partage de fichiers images (photographies).

Tous ces services, accessibles via un seul compte, sont très révélateurs des usages personnels&nbsp;: partager facilement ses photos dans un cercle familial, ne plus avoir à trier ses courriels grâce à un espace de stockage quasi inépuisable pour la majorité des utilisateurs, pouvoir éditer à plusieurs des documents qui resteront stockés en ligne et accessibles à tout moment, etc.

Sur Internet, de grandes entreprises proposent de tels outils gratuitement, les mettant ainsi à disposition de millions d'utilisateurs à travers le monde. Les plus connues disposent souvent d'une version gratuite (imposant de la publicité en échange du service et des informations profilées des utilisateurs) et d'une version payante.


### Mise en garde

Aussi séduisants qu'ils puissent être, les services de messagerie en ligne gratuits ou payants, sont souvent sous le feu des projecteurs. Ils supposent tous un certain degré de confiance de la part des utilisateurs qui acceptent des clauses de confidentialité parfois abusives, ou, plus simplement, ne remplissent pas leur devoir en matière de garantie de sécurité des données.

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Si c'est gratuit, c'est vous le produit !</strong></p>

En particulier lorsque le service est gratuit, un utilisateur doit toujours être conscient qu'en déléguant à un tiers la gestion de son courrier personnel ou de n'importe quelles autres données, il s'engage à prendre le risque de les perdre ou de laisser à ce tiers la possibilité de les analyser pour en tirer des informations plus ou moins personnelles en vue d'une utilisation commerciale, ou pour le compte d'autrui (comme une agence de renseignements).

</div>

Par ailleurs, suivant les pays où sont stockées ces données (l'emplacement des serveurs), ces dernières ne sont plus  forcément sous la même juridiction que l'émetteur&nbsp;; là aussi des dispositions doivent être prises par les utilisateurs afin de se prémunir d'un éventuel défaut juridique ou d'un droit de regard exercé par un gouvernement mal intentionné.


### Effacer mes traces


Les premières précautions à prendre lorsqu'on utilise un *webmail* est d'effacer ses traces une fois le travail terminé, tout particulièrement si la consultation a lieu depuis un cybercafé ou un ordinateur qui n'est pas à soi. En effet, puisqu'il est consulté depuis un navigateur, ce dernier conserve certaines données.

Il faut donc penser à&nbsp;:

* effacer l'historique de navigation et les cookies,
* ne pas accepter de stocker en mémoire les mots de passe et les champs de formulaires,
* penser à se déconnecter du service (clôturer la session de travail).

Pour cela un navigateur comme [Firefox](https://www.mozilla.org/fr/firefox/new/) propose des outils efficaces&nbsp;:

* utiliser le mode de navigation privée (menu `Fichier > Nouvelle fenêtre de navigation privée`),
* supprimer tout l'historique en une seule fois (Menu `Historique > Supprimer l'historique récent`, sélectionner la totalité de l'intervalle à effacer et cocher toutes les cases cookies, cache, préférences de site, etc.),
* installer des extensions pour limiter et monitorer la surveillance par cookies, tel [Privacy Badger](https://www.eff.org/fr/privacybadger).

Enfin, tous les systèmes d'exploitation (GNU/Linux, MacOs, Windows, Android, etc.) permettent de se connecter avec un nom d'utilisateur et un mot de passe. Que vous utilisiez un ordinateur portable, une tablette ou un ordinateur fixe, il est primordial de ne pas laisser n'importe qui accéder à votre session et, donc, à tous vos documents, y compris le navigateur et son historique. Il ne faut donc pas hésiter à utiliser ce système supplémentaire de protection qu'est l'ouverture de session avec mot de passe.


### Maîtriser mes connexions

La plupart des services en ligne, qu'il s'agisse de messagerie, de réseaux sociaux ou de stockage *cloud*, permettent de récupérer les mots de passe oubliés par une procédure basée sur l'envoi automatique d'un courriel.  Bien que la procédure soit plus ou moins complexe selon le service concerné, il est important de ne pas négliger les étapes de sécurisation proposées lors de l'ouverture du compte&nbsp;:

* les phrases/questions secrètes,
* l'adresse courriel de secours (si possible),
* le niveau de complexité du mot/phrase de passe.

Concernant le mot de passe, deux erreurs courantes sont à éviter&nbsp;:

* utiliser le même mot de passe pour plusieurs voire tous les services en ligne utilisés,
* utiliser un mot de passe trop simple.

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Se connecter en deux étapes</strong></p>

Certains services proposent une méthode dite de «&nbsp;validation en deux étapes&nbsp;» pour se connecter à sa boite aux lettres. Elle consiste par exemple, à chaque connexion, d'entrer d'une part le login et le mot de passe et, d'autre part, un code envoyé sur le téléphone portable servant à valider cette connexion. Cela nécessite de donner son numéro de téléphone portable à un tiers et d'avoir son téléphone à disposition lors de la connexion, mais elle complique efficacement toute tentative frauduleuse.
</div>

Reportez-vous au chapitre 5 pour de plus amples détails sur les mots de passe.



## Les logiciels de messagerie (clients de courriel)


Un logiciel de messagerie est appelé un *client* de courrier électronique. Les *webmails* dont il vient d'être question à la section précédente en font partie. Ils sont reconnus comme étant des *clients légers*, parce qu'ils fonctionnent sur des serveurs en tant qu'applications accessibles à distance. Ils se distinguent des *clients lourds*, c'est à dire des logiciels qui s'installent localement sur un ordinateur et qui se chargent de récupérer les messages depuis un serveur de courriels.

Un client lourd : ourquoi utiliser un tel outil&nbsp;?

* premièrement parce que vous n'êtes pas obligé-e d'être connecté-e en permanence. Si vous êtes en déplacement, sans connexion, vous pouvez toujours rédiger vos courriels, les sauvegarder et les envoyer une fois connecté-e&nbsp;;
* en second lieu, vous pouvez configurer votre client de courriel comme *vous* le voulez. Par exemple vous pouvez utiliser un système de chiffrement à vous, tel GnuPG (cf. plus bas)&nbsp;;
* enfin, vous pouvez gérer plusieurs comptes à la fois, en gardant la même interface. Vous pouvez même copier des messages d'un compte à l'autre, configurer vos dossiers, etc.

Le logiciel [Mozilla Thunderbird](https://www.mozilla.org/fr/thunderbird/) est sans doute l'un des plus connus. Il est distribué sous licence libre et sous ce nom depuis 2003, traduit dans plus de cinquante langues, et compatible avec les systèmes d'exploitation courants tels GNU/Linux, MacOS et Windows. Pour l'utiliser, vous devez déjà avoir une adresse courriel (vous pouvez la créer lors de la première ouverture du logiciel).

Au premier lancement du logiciel, ou lorsque vous souhaitez configurer un compte de messagerie, Thunderbird cherche à détecter automatiquement les protocoles de communication disponibles sur le serveur correspondant à votre adresse courriel. Ainsi, si vous avez entré une adresse du type `Jean.dupont@tictacmail.com`, Thunderbird tentera de se connecter au serveur de messagerie `tictacmail.com` et détectera les paramètres de connexion pour envoyer et recevoir du courrier, ainsi que les méthodes disponibles pour le faire (les protocoles).

Quelques explications s'imposent.

### Configurer un client de courriel local

Pour envoyer et recevoir du courrier, il faut indiquer au logiciel de messagerie les informations qui lui permettront d'opérer&nbsp;:

* se connecter au serveur avec un nom de compte et un mot de passe,
* savoir avec quel protocole se connecter pour l'envoi et la réception, et gérer les messages en fonction de cette information.

De manière générale, pour configurer son logiciel de messagerie, vous pouvez vous satisfaire de la configuration qui vous est proposée par défaut. La plupart du temps le service de messagerie auquel vous êtes abonné-e diffuse dans ses pages d'aide toutes les informations nécessaires à la configuration d'un client de messagerie —&nbsp;adresse du serveur, protocoles disponibles, ports à configurer, ainsi&nbsp;:

* SMTP (*simple mail transfert protocol*): utilisé pour le transfert du courrier électronique vers le serveur de messagerie. Le logiciel utilise le nom du compte et le mot de passe, soumet une requête au serveur et ce dernier reçoit le message.
* POP (*post office protocol*): il est utilisé pour récupérer les messages depuis le serveur. Les messages sont alors *téléchargés*. Il est possible de configurer le logiciel de messagerie avec le protocole POP pour qu'il laisse ou non une copie des messages sur le serveur pendant une période déterminée.
* IMAP (*internet message access protocol*)&nbsp;: il est utilisé pour synchroniser les messages sur le serveur et sur le logiciel de messagerie. Autrement dit, il «&nbsp;laisse&nbsp;» les messages sur le serveur tant qu'on ne les supprime pas explicitement, il permet de travailler directement sur le serveur, de manipuler les messages et les dossiers.
* SSL (*secure sockets layer*) et TLS (*transport layer security*)&nbsp;: il s'agit de protocoles permettant de sécuriser les échanges sur internet. Ils ne concernent pas uniquement la messagerie, mais ils permettent de définir le niveau de sécurité avec lequel le logiciel peut se connecter au serveur pour l'envoi et la réception du courrier (et limiter ainsi toute possibilité d'interception).
* Les ports. Au moment de configurer votre client de messagerie, vous remarquerez certainement qu'à chaque protocole et son niveau de sécurité correspondent des ports logiques particuliers. Un port est à considérer comme une porte (c'est d'ailleurs son sens premier) empruntée par un programme informatique pour écouter ou émettre des informations. Pour l'envoi, par défaut le protocole SMTP émet sur le port 25. Mais si l'on souhaite sécuriser cet envoi avec SSL, il faut alors utiliser les ports 587 (ou parfois 465 selon le serveur de messagerie). Le protocole POP utilise le port 110 et de manière sécurisée le port 993. Le protocole IMAP utilise le port 143 et de manière sécurisée le port 993.


### Exemple de configuration

Je dispose d'une adresse courriel `jean.dupont@youkoulele.fr et je souhaite pouvoir utiliser ma messagerie en IMAP avec une connexion sécurisée.

Je me rends sur les pages de documentation de `youkoulele.fr` et j'apprends que pour configurer en IMAP, il me faut entrer ces informations pour une connexion SSL&nbsp;:

* IMAP port 993 serveur `mail.youkoulele.fr`
* SMTP port 587 serveur `mail.youkoulele.fr`

Lors de la configuration d'un nouveau compte avec l'assistant de Thunderbird&nbsp;:

![Nouveau compte sur Thunderbird](images/config_thunderbirdnouveaucompte.png)

Dans la page de gestion des comptes (`Édition > Paramètres des comptes`)&nbsp;:

![Thunderbird&nbsp;: paramètres (1)](images/configthunderbird2.png)

![Thunderbird&nbsp;: paramètres (2)](images/configthunderbird3.png)



## Les usages

Tout particulièrement lorsqu'il s'agit de communiquer via une messagerie électronique, il est primordial de respecter quelques règles d'usage. La Nétiquette constitue l'ensemble de ces règles informelles instituées en 1995, alors même qu'à l'arrivée d'Internet dans les foyers correspondait une multiplication exponentielle des  communications par messagerie électronique, à titre individuel ou professionnel. Toutes les informations concernant la Nétiquette sont trouvables sur la page Wikipédia qui lui est consacrée (voir [Nétiquette (Wikipédia)](http://fr.wikipedia.org/wiki/N%C3%A9tiquette)). Nous résumerons ici quelques points importants concernant le courrier électronique.



### Peaufiner la configuration de mon client de messagerie

Une fois entrées les informations permettant de recevoir et envoyer du courrier,  quelques petites manipulations restent encore à faire.

Dans la section consacrée aux options de rédaction des messages (dans Thunderbird, elle est nommée «&nbsp;rédaction et adressage&nbsp;», dans un *webmail* ces réglages sont accessibles via les options de votre compte), il est important de bien choisir la manière dont les messages seront rédigés.

* Utiliser le HTML pour envoyer un message vous permettra d'effectuer une mise en page sur votre contenu comme par exemple ajouter des couleurs, faire des effets de taille de caractères, etc. Néanmoins, si vous envoyez tous vos messages en HTML, c'est à dire comme une page web, soyez sûrs que votre correspondant pourra les lire dans la mise en page que vous avez souhaitée. Selon son logiciel et la manière dont il l'a configuré, cela ne sera peut-être pas le cas. De même, il n'est pas nécessaire d'envoyer un message en HTML accompagné de multiples images non pertinentes, comme une image de signature ou une image de fond de style «&nbsp;papier à lettre&nbsp;». Il est donc souvent préférable de rédiger ses messages en mode texte brut (*plain text*).
* Lorsque qu'on répond à un message, il est de bon ton de commencer à l'écrire *en dessous* du message original. Cela est particulièrement utile lorsque le fil de discussion comporte plusieurs réponses&nbsp;: on peut alors suivre aisément le déroulé de la conversation de haut en bas. Ne pas hésiter, aussi, à  supprimer les contenus non pertinents dans les messages précédent le vôtre, en particulier si vous répondez à un point précis de la discussion.

![Thunderbird&nbsp;: configuration de la messagerie (1)](images/configthunderbird4.png)

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Une identité lisible</strong></p>

Il est préférable d'annoncer vos noms et prénoms dans les options qui concernent votre identité. Ainsi votre correspondant pourra lire votre adresse ainsi&nbsp;: <code>Jean Dupont <jean.dupont@youkoulele.fr></code> au lieu de ne voir que votre adresse électronique. Ceci est particulièrement utile si votre adresse électronique ne reflète pas votre identité, comme <code>kikoo88@youkoulele.fr</code>, ce qui n'est pas conseillé sur un compte destiné à envoyer des correspondances officielles.

</div>

![Thunderbird&nbsp;: configuration de la messagerie (2)](images/configthunderbird5.png)


### Écrire mes messages

La Nétiquette citée plus haut vous permettra de retenir les pratiques courantes et les règles de respect en matière d'écriture de correspondance électronique. Néanmoins les quelques principes suivants sont toujours utiles à retenir.

Pour envoyer un courrier électronique, les champs suivants sont toujours demandés ou proposés&nbsp;:

* l'adresse ou les adresses du(es) destinataire(s) (champ `À:`),
* l'adresse ou les adresses du(es) destinataire(s) en copie (champ `Cc:`, copie carbone),
* l'adresse ou les adresses du(es) destinataire(s) en copie invisible (champ `Bcc:` ou `Cci`, Blind Carbon Copy, ou Copie Carbone Invisible),
* l'objet du message,
* le contenu du message.

Il faut savoir utiliser à bon escient les champs relatifs à la destination, en particulier la copie. La plupart du temps il est inutile d'envoyer le même message à une dizaine de personnes en supposant que, parce qu'elles reçoivent le message, elles le retiendront ou souhaitent obligatoirement le recevoir. Des pratiques non judicieuses sont souvent rencontrées dans les milieux professionnels où les messages collectifs génèrent du stress sous prétexte de dédouaner l'émetteur. Parfois, rien ne vaut un bon vieux coup de téléphone ou tout simplement parler autour d'un café, en passant.

La copie invisible est elle aussi à utiliser avec discernement. Parfois les non-dits peuvent être source de conflit, ou au contraire le choix des destinataires visibles et invisibles peut s'avérer diplomatique. Surtout, n'envoyez jamais un courriel à une liste de destinataires n'ayant aucun rapport entre eux sans utiliser la copie invisible&nbsp;: certains d'entre eux vous ont donné leur adresse courriel sans pour autant vous autoriser à la divulguer à des destinataires qui leurs sont inconnus&nbsp;!

L'objet du message, quant à lui, doit toujours refléter efficacement son contenu. Il permet au destinataire de classer ses messages et choisir le bon moment pour les lire. C'est l'objet qui détermine le fil de discussion. Si celle-ci dévie trop de son axe original, il devient judicieux de créer un nouveau courriel et définir un objet approprié pour créer un nouveau fil. Par facilité, ne faites pas une réponse à un courriel pour parler d'autre chose.


### Les pièces jointes

Les pièces jointes sont les fichiers que l'on envoie à son destinataire en même temps que le message. Là aussi, quelques précautions d'usage s'imposent&nbsp;:

* s'assurer que le destinataire est bien en mesure de lire le fichier. Cela signifie qu'il faut garantir l'interopérabilité (voir chapitre 1)&nbsp;: ne pas supposer que le destinataire dispose du même logiciel que vous et dans sa même version. Par exemple, un document circulera mieux en version PDF que dans sa version originale issue d'un logiciel qui n'admet pas les standards ouverts.
* se demander si le fichier joint est vraiment utile&nbsp;: son contenu ne pourrait-il pas figurer directement dans le corps du message&nbsp;? Ceci afin de ne pas obliger le destinataire à télécharger le fichier, l'ouvrir avec un logiciel, et faire plusieurs opérations superflues.
* si le fichier est uniquement destiné à un affichage sur écran, comme une photo par exemple, il est inutile de l'envoyer dans son format d'origine et en haute résolution. Compresser une image ne prend que quelques instants et facilite son chargement.
* avant d'envoyer un fichier à plusieurs destinataires, il faut se demander si tous les destinataires ont besoin de ce fichier, en particulier si l'espace disponible dans leur boîte à lettres est restreint.
* enfin, on pourra tout simplement se demander si l'envoi d'un fichier en pièce jointe ne serait pas mieux remplacé par un simple lien pointant vers ce même fichier stocké sur un serveur *cloud*. En effet, particulièrement lorsqu'il est volumineux, il est possible de stocker un fichier dans un espace de stockage en ligne et de le rendre accessible via une simple URL que l'on communique alors dans le message. Dans ce cas, les destinataires peuvent alors avoir le choix de télécharger ou non le fichier sans qu'il n'encombre leurs boîtes. L'association Framasoft propose par exemple deux services permettant d'échanger des fichiers en toute confidentialité&nbsp;: [Framapic](https://framapic.org/) vous permet d'échanger une ou plusieurs images sous forme d'album, [Framadrop](https://framadrop.org/) vous permet d'envoyer un fichier volumineux. Dans les deux cas, ce sont de simples liens que vous échangez par courriel.

## Que dois-je savoir en plus&nbsp;?


### Chiffrement des messages

Il peut être parfois préférable d'envoyer des messages que seul le destinataire est capable de lire. Pour cela, la plupart des logiciels clients de courrier électronique disposent de fonctionnalités de chiffrement. La meilleure solution est basée sur GnuGPG (Gnu Privacy Guard), une implémentation libre de OpenPGP (Pretty good Privacy) et fonctionne via un système de clés privées et clés publiques (voir chapitre 5).

Pour utiliser ce système, certains logiciels clients disposent par défaut de tous les outils nécessaires pour configurer et gérer vos signatures numériques. Une extension connue pour Thunderbird se nomme *Enigmail*, et permet de chiffrer et déchiffrer les messages de manière automatique.


### Bien choisir mon service de messagerie

Tout utilisateur disposant d'un abonnement auprès d'un fournisseur d'accès Internet dispose, pour ainsi dire «&nbsp;par défaut&nbsp;», d'un compte de messagerie dont le mot de passe est acquis en même temps que le contrat qui le lie à son fournisseur d'accès. Ce compte de messagerie est utile pour le fournisseur d'accès afin de communiquer avec son abonné. Cependant, rien n'oblige ce dernier à utiliser ce compte.

Plusieurs raisons peuvent justifier un autre choix&nbsp;: la qualité du service, certains protocoles (cf. ci-dessous) non accessibles pour l'abonnement contracté, manque de fonctionnalités, l'utilisateur dispose déjà d'une messagerie et ne tient pas à en changer en fonction de ses abonnements successifs, etc.

Plusieurs idées reçues doivent pourtant être combattues&nbsp;:

* croire qu'un service de messagerie doit être gratuit,
* croire que la gratuité n'engage à rien,
* penser que plus le service est connu, meilleur il est,
* ignorer que le choix d'un hébergement (pour sa messagerie ou autre chose) peut aussi être un choix de conviction.

Au moins deux principes doivent guider le choix d'un service&nbsp;:

* l'affichage clair de ses positions vis-à-vis de la sécurité et de la confidentialité des données que vous confiez,
* les fonctionnalités qu'ils propose et ce que l'on est éventuellement prêt à payer (ou sacrifier) pour cela.

Dans la mesure du possible, il est préférable d'évaluer la *fiabilité technique et éthique*   du service, l'identité de l'entreprise ou de l'association qui le propose, et enfin, seulement, le coût éventuel.

Les exemples qui suivent concernent les Conditions Générales d'Utilisation (CGU) de trois acteurs spécialisés dans l'hébergement de services, dont le courrier électronique.

Extrait des CGU de l'association La Mère Zaclys (en date du 17/01/2017)&nbsp;:

>  La Mère Zaclys est une association loi 1901 à but non lucratif qui propose des services en ligne tel qu'un album photos, un cloud, une boite email, un service de partage de fichiers et un lecteur de flux RSS.  [...] Le site est hébergé sur des serveurs français (chez OVH à Roubaix), développé et maintenu sur le territoire français (Franche-comté) et utilise exclusivement des logiciels libres (linux). Le respect de votre liberté et de vos droits est pour nous une priorité absolue. Contrairement à d'autres sites bien connus, nous garantissons vos libertés et vos droits de propriété intellectuelle&nbsp;: La mère Zaclys ne s'accorde aucun droit d’utilisation, de reproduction, de modification, de création d’œuvres dérivées, de communication, de publication, de représentation, d’affichage, de distribution... de vos contenus.  Votre contenu reste votre contenu et en aucun cas le nôtre.

Extrait des CGU de l'entreprise MailObject (Netcourrier) (en date du 17/01/2017)&nbsp;:

> L'entreprise MailObject, est une entreprise française ayant développé une technologie du même nom. Elle fournit un service baptisé Netcourrier (Net-C), en version gratuite (limitée) ou payante (avec plus de fonctionnalités), pour les particuliers, les familles ou les entreprises. Bien que proposant un service de stockage cloud, son activité se concentre sur le domaine de la messagerie. Elle revendique, via une [charte](http://www.netcourrier.com/netc/fr/vie-privee-et-charte-net-c.php) relative à la vie privée, son engagement pour une politique non intrusive et la défense des principes d'autonomie et de confidentialité.

Extraits des CGU de l'entreprise Google (en date du 17/01/2017)&nbsp;:

> Lorsque vous utilisez nos services ou que vous affichez des contenus fournis par Google, nous collectons et stockons des informations dans les fichiers journaux de nos serveurs. Cela comprend&nbsp;:
>
>* la façon dont vous avez utilisé le service concerné, telles que vos requêtes de recherche.
> * des données relatives aux communications téléphoniques, comme votre numéro de téléphone, celui de l’appelant, les numéros de transfert, l’heure et la date des appels, leur durée, les données de routage des SMS et les types d’appels.
>   * votre adresse IP.
>   * des données relatives aux événements liés à l’appareil que vous utilisez, tels que plantages, activité du système, paramètres du matériel, type et langue de votre navigateur, date et heure de la requête et URL de provenance.
>   * des cookies permettant d’identifier votre navigateur ou votre Compte Google de façon unique.
>
> [...] Lorsque vous utilisez des services Google, nous sommes susceptibles de collecter et traiter des données relatives à votre position exacte.
> [...] Lorsque vous contactez Google, nous conservons un enregistrement de votre communication afin de mieux résoudre les problèmes que vous rencontrez.
> [...] Nos systèmes automatisés analysent vos contenus (y compris les e-mails) afin de vous proposer des fonctionnalités personnalisées sur les produits, telles que des résultats de recherche personnalisés, des publicités sur mesure, et la détection de spams et de logiciels malveillants.


Les exemples qui précèdent illustrent des conceptions différentes d'une (ou plusieurs) proposition(s) de service(s). On comprend aisément que dans la mesure où ces données privées sont exploitées, et génèrent du bénéfice, le fournisseur peut offrir gratuitement et à l'échelle la plus large possible l'usage de ses services. Au contraire, dans le cas où ces données privées sont sanctuarisées par le fournisseur, faire payer le service est pour lui l'une des seules ressources pour pérenniser son activité. C'est en particulier le cas pour des associations qui, parce qu'elles bénéficient de l'engagement humain des bénévoles, proposent ce genre de services pour des coûts relativement bas visant à couvrir les frais d'infrastructure (reportez-vous au chapitre 4 où il est question du collectif CHATONS).

Les structures suivantes sont des alternatives fiables et crédibles face aux «&nbsp;géants&nbsp;» les plus connus du web. Elles proposent des CGU respectueuses des données des utilisateurs. Certaines sont des entreprises, d'autres des structures sans but lucratif. Toutes font reposer leurs services sur des solutions de logiciels libres. Consultez leurs CGU et prenez l'habitude de procéder ainsi pour choisir votre fournisseur de service, en connaissance de cause. Bien sûr, un fournisseur pourra toujours changer ces CGU, voire ne pas les respecter. Par ailleurs, n'oubliez jamais qu'à moins d'héberger vous même votre messagerie, vous devez faire confiance à un tiers. En la matière, la transparence est donc nécessaire.

| Nom                                        | Webmail | IMAP, POP, SMTP / SSL | Chiffrement |
|:------------------------------------------ |:-------:|:---------------------:|:-----------:|
| [La Mère Zaclys](https://mail.zaclys.com/) | X       | X                     | X           |
| [Riseup](https://riseup.net/fr)            | X       | X                     | X           |
| [Ouvaton](https://ouvaton.coop)            | X       | X                     | X           |
| [Lautre.net](https://www.lautre.net/)      | X       | X                     | X           |
| [Infini](http://www.infini.fr)             | X       | X                     | X           |
| [Net-C](https://www.net-c.com/)            | X       | X                     | X           |
| [Netcourrier](http://www.netcourrier.com)  | X       | X                     | X X         |
| [Mailfence](https://mailfence.com)         | X       | X                     | X X X       |
| [Protonmail](https://protonmail.com)       | X       | N                     | X X X       |


Notons que certains fournisseurs cités dans ce tableau proposent aussi les protocoles Exchange/ActiveSync (Microsoft).

### Héberger mon propre serveur courriel

Nous ne saurions terminer cette section sans mentionner l'auto-hébergement de son propre serveur de messagerie. Cette possibilité est réservée à des utilisateurs avancés et tout particulièrement attentifs aux questions de sécurité (car il faut alors assumer seul la protection de ses données). Mais peut-être autour de vous des personnes peuvent prendre en charge un tel serveur, pour le compte de la famille, d'un groupe, d'une association.

En effet, pour envoyer et recevoir des courriers électroniques, il n'y a aucune obligation de stocker ses messages auprès d'un service tiers&nbsp;: cela est possible chez soi, sur un ordinateur configuré pour cela, ou sur un serveur distant loué à cette occasion (ce qui revient à utiliser un service tiers mais en gardant la main sur les solutions logicielles utilisées).

De nombreux tutoriels sont présents sur la toile, et montrent comment configurer *postfix* ou *sendmail*, des logiciels libres permettant de gérer un service de messagerie. Par ailleurs, les facilités se sont récemment multipliées. On peut mentionner par exemple le marché des *nano-computers*, des mini ordinateurs consommant très peu d'énergie et vendus à très bas prix, permettant justement de mettre en place un serveur chez soi sans mobiliser une grosse machine pour cela. Enfin des systèmes de gestion de serveur permettent de faciliter la configuration des logiciels utilisés pour obtenir des solutions fiables et basées sur des logiciels libres. On peut citer [Yunohost](https://yunohost.org/#/) et [Cozy Cloud](https://cozy.io/fr/), qui permettent bien plus qu'un serveur de courriel.

### Mes courriels avec un smartphone

Par défaut, les smartphones embarquent des applications de gestion de courrier électronique. Pour les systèmes Android, il y a la plupart du temps installés d'emblée le client de courriel choisi par le fabricant et l'application Gmail. Les deux permettent de se connecter à un serveur de son choix, y compris avec différents protocoles (pop, imap, exchange, etc.), et de rapatrier son courriel. Or, les interfaces proposées peuvent ne pas vous convenir, mais surtout vous ne maîtrisez pas la manière dont ces courrielleurs se connectent à votre boite à courriel.

Des enquêtes approfondies montrent régulièrement les failles de ces clients de courriel sur appareils mobiles. Parfois, plus que de simples failles de sécurité, ce sont de véritables intrusions dont il s'agit, comme le montre Charly dans [une étude technique](http://linuxfr.org/users/charlycha/journaux/mefiez-vous-des-applications-de-courriel-sur-mobile) mais néanmoins vulgarisée. L'étude de Charly concerne deux applications connues qui ne se contentent pas de rapatrier le courriel. En réalité, elles s'approprient et envoient vos login et mot de passe sur les serveurs du fournisseur de l'application et correspondent alors directement avec les serveurs de vos courriels, qu'il s'agisse de votre boîte mail personnelle ou celle de votre entreprise. En d'autres termes&nbsp;: n'utilisez pas et n'installez pas n'importe quel client de courriel sur votre mobile&nbsp;!

L'application libre [K9-Mail](https://k9mail.github.io/) fait toutefois office de référence en la matière et bénéficie d'un développement très actif. On peut dire que K9-Mail s'utilise et se configure comme un client sur ordinateur. Si vous avez lu ci-dessus l'exemple de configuration de Thunderbird, vous pouvez reproduire la méthode avec K9-Mail, en plus de plein d'autres options. Par ailleurs, K9-Mail intègre une autre application, [OpenKeyCHain](https://www.openkeychain.org/), qui permet d'utiliser PGP pour chiffrer ses communications (cf. le chapitre 5).


## La messagerie instantanée

On appelle messagerie instantanée une forme de dialogue en ligne, autrement nommé *chat* (anglicisme francisé en *tchat*). Il se pratique par l'intermédiaire de terminaux (ordinateur fixe, smartphone, tablette) connectés au même réseau. La messagerie instantanée se pratique à l'aide d'un logiciel client connecté à un serveur. Le logiciel peut être un client installé localement sur votre machine ou bien sous forme d'un service web, comme par exemple Gtalk intégré dans les services de Google.

Comme c'est le cas pour le courriel, les services de messagerie instantanée utilisent des protocoles particuliers. Certains sont des protocoles fermés, c'est le cas de Microsoft Messenger ou Yahoo!Messenger. Leur principal défaut est de maintenir les utilisateurs sur les réseaux qui utilisent ces protocoles avec l'impossibilité de pouvoir discuter avec des utilisateurs utilisant d'autres protocoles. On dit alors qu'ils sont *non-interopérables*.

Un autre défaut est relatif à la sécurité&nbsp;: en tant que systèmes fermés, seule la confiance que vous avez envers le fournisseur du service vous permet de mesurer le degré de confiance que vous pouvez accorder pour transmettre des informations (texte, vidéo, audio) à travers ces réseaux. Dans la mesure où la confiance numérique nécessite une expertise, il vaut mieux se tourner vers des standards ouverts, qui, eux, bénéficient de l'expertise de leurs utilisateurs.

Ces systèmes fermés ont eu leurs heures de  gloire à l'époque où les alternatives utilisant des standards ouverts ne pouvaient pas encore assurer toutes les fonctionnalités offertes par ces systèmes propriétaires, comme par exemple le vidéo-chat. Aujourd'hui, certains standards comme XMPP permettent de faire des discussions privées et de salon, supportent la vidéo et l'audio, et utilisent des protocoles de sécurité sérieux. Pour ce qui suit, nous allons nous pencher sur l'un des plus connus&nbsp;: XMPP.

### Utiliser XMPP

Le protocole [XMPP](https://xmpp.org) (Extensible Messaging and Presence Protocol) est en fait un ensemble de protocoles dédié à l'échange en ligne (instantané ou non). Le standard ouvert XMPP a été déployé par l'Internet Engineering Task Force (IETF) sur la base de la reconnaissance du protocole Jabber, créé par Jérémie Miller. L'intérêt de Jabber / XMPP était double&nbsp;:

1. proposer une plate-forme de serveurs de messagerie instantanée capable de communiquer de manière transparente avec tous les autres systèmes de messagerie (d'où l'intérêt de placer ce système sous licence libre pour que les autres acteurs puissent s'en emparer),
2. créer un système *décentralisé*, c'est-à-dire permettre aux utilisateurs de créer un compte sur n'importe quel serveur XMPP et pouvoir communiquer avec tous les comptes situés sur n'importe quel serveur (à la différence des comptes comme Skype qui nécessitent d'avoir un compte sur un serveur en particulier).

L'avantage est que si un serveur tombe ou se trouve censuré (ce qui peut arriver dans une dictature par exemple), cela n'empêche par pour autant les échanges avec XMPP. Alors que si Microsoft décide d'arrêter ses serveurs Skype, plus aucun compte ne pourra communiquer.

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>To be or not to be</strong></p>

De nombreux acteurs industriels ou de services utilisent désormais XMPP et le proposent aux utilisateurs. Il est possible, par exemple avec un compte Gtalk de communiquer via XMPP. Facebook, en revanche, après avoir ouvert temporairement l'accès à XMPP, a décidé de le fermer en 2015, obligeant ainsi ses utilisateurs à utiliser exclusivement l'application de Facebook.
</div>

Pour utiliser XMPP l'idéal est de trouver un logiciel client qui permet de créer un compte et de l'utiliser d'emblée. Pour cela vous pouvez choisir [Pidgin](https://www.pidgin.im/), qui a aussi l'avantage de proposer beaucoup d'autres protocoles (y compris non libres) et de regrouper ainsi tous vos comptes de messagerie instantanée.

Après avoir installé Pidgin, créez un nouveau compte et choisissez le protocole à utiliser. Dans l'illustration suivante, le choix porte sur XMPP.



![Configuration de Pidgin pour utiliser XMPP](images/pidgin1.png)

Choisissez ensuite un nom d'utilisateur et un *domaine*, c'est-à-dire le serveur Jabber/XMPP sur lequel vous avez un compte. Si tel n'est pas le cas, vous pouvez cocher en bas la case `Créer ce nouveau compte sur le serveur`.

Si vous ne connaissez pas de serveur, vous pouvez choisir l'un de ceux proposés sur la [liste des serveurs publics](https://xmpp.net/directory.php). Par exemple&nbsp;: le [serveur de l'Apinc](https://jabber.apinc.org/) ou celui de [La Quadrature du Net](https://jabber.lqdn.fr/).

Ensuite, si seulement vous en avez besoin, et selon les spécifications expliquées sur la page du serveur que vous choisissez (par défaut ne faites rien), allez dans l'onglet `Avancé` et choisissez la méthode de chiffrement, le port et le proxy si besoin.

![Configuration avancée de Pidgin](images/pidgin2.png)


### Maîtriser ma messagerie instantanée

De nombreuses extensions sont disponibles pour Pidgin. Elles sont listées sur [la page wiki du site officiel](https://developer.pidgin.im/wiki/ThirdPartyPlugins). Pour une communication utilisant le chiffrement puissant de type OTR (Off-the Record Messaging), il faut installer le plugin *Pidgin-otr* (sur votre poste comme celui du correspondant). Pour comprendre comment procéder, vous pouvez suivre [ce tutoriel](https://securityinabox.org/fr/guide/pidgin/windows/).

La messagerie instantanée n'est pas réservée aux postes fixes. Sur tablette ou smartphone, il est aussi intéressant de pouvoir disposer d'outils qui permettent de communiquer rapidement et en toute sécurité.

L'application [Xabber](https://www.xabber.com/), par exemple, est un logiciel libre utilisant le protocole XMPP et permet de chiffrer facilement ses conversations avec OTR. Xabber permet aussi de se connecter à un compte Gtalk. La configuration du compte diffère très légèrement de Pidgin.

Dans le champ `Identifiant` entrez `votrelogin@nomduserveur.xx`. Votre serveur est le serveur sur lequel vous avez déjà un compte ou bien, si vous n'y avez pas encore de compte, vous pouvez le créer d'emblée en cochant la case `Créer un nouveau compte`. Si vous activez OTR pour ce compte, vous serez invité à installer un second logiciel nommé [Orbot](https://guardianproject.info/apps/orbot/), si cela n'est pas déjà fait. Il s'agit de l'application TOR pour smartphone.

On peut noter que, en matière de messagerie instantanée sur plate-forme mobile, l'une des applications les plus sécurisées n'utilise pas XMPP. Elle a pour nom [Signal](https://whispersystems.org), un logiciel libre développé par Open Whisper Systems. Signal utilise un protocole cryptographique justement nommé Signal, qui assure un chiffrement de bout en bout&nbsp;: seuls les utilisateurs de Signal peuvent lire leurs messages chiffrés. Pour l'utiliser, il faut donc que les correspondants disposent de la même application. Signal assure aussi de la VoIP (le téléphone via Internet), toujours de manière chiffrée avec le protocole [ZRTP](https://fr.wikipedia.org/wiki/ZRTP). Un client Signal pour ordinateur est aussi disponible, il se nomme [Signal Desktop](https://whispersystems.org/blog/signal-desktop/) et fonctionne en tant qu'extension à Google Chrome.

Si vous voulez utiliser de la VoIP en utilisant XMPP (et donc votre compte) vous pouvez utiliser l'application [Jitsi](https://jitsi.org/), un client développé par l'Université de Strasbourg. Vous pouvez ainsi faire de la téléphonie IP depuis votre mobile ou votre ordinateur. Comme Pidgin pour le tchat, Jitsi est aussi ouvert à d'autres protocoles libres ou non.

[^11]: Cette section est reprise en partie selon une section que l'auteur a rédigé dans le cadre d'un projet de Framabook intitulé *Guide de survie numérique*.
