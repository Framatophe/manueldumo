# Libertés numériques

— Guide de bonnes pratiques à l'usage des DuMo

Par : Framatophe

Copyright : mars 2017

Mise à jour : août 2017

Licence: [Art Libre](http://artlibre.org/)
